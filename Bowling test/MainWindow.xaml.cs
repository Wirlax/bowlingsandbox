﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Timers;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Bowling_test
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Boule bouleourge;
        public MainWindow()
        {
            InitializeComponent();
            bouleourge = new Boule(Piste);
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DateTime tempsmaintenant = DateTime.Now;
            bouleourge.faire_bouger_mru((2), tempsmaintenant);
            Piste.Children.Add(bouleourge.myPath);

        }
    }
    public class Boule
    {
        public EllipseGeometry boule;
        public Path myPath;
        int a;
        double v;
        double x = 0;
        double y = 0;
        int xzero, yzero;
        public Boule(Canvas canvas)
        {
            EllipseGeometry boule = new EllipseGeometry();
            boule.RadiusX = 25;
            boule.RadiusY = 25;
            boule.Center = new Point(150, 150);
            myPath = new Path();
            myPath.Fill = Brushes.Gold;
            myPath.Stroke = Brushes.Black;
            myPath.StrokeThickness = 1;
            myPath.Data = boule;
            canvas.Children.Add(myPath);
            /*boule.SetValue(Canvas.LeftProperty, (double)150);
            boule.SetValue(Canvas.TopProperty, (double)150);*/
            this.boule = boule;
            this.a = 0;
            this.v = 0;
            this.xzero = 150;
            this.yzero = 150;
        }
        public EllipseGeometry faire_bouger_mru(double V, DateTime T)
        {
            DateTime tempsmaintenant = DateTime.Now;
            int Differencetemps = tempsmaintenant.Subtract(T).Seconds;
            x = V * Differencetemps + xzero;
            boule.Center = new Point(x, 150);
            return this.boule;
        }
    }
}
