﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Bowling_test;
namespace Classes_Bowling
{
    class draw
    {
        public static void circle(int x, int y, int width, int height, Canvas cv)
        {

            Ellipse circle = new Ellipse()
            {
                Width = width,
                Height = height,
                Stroke = Brushes.Red,
                StrokeThickness = 6
            };

            cv.Children.Add(circle);

            circle.SetValue(Canvas.LeftProperty, (double)x);
            circle.SetValue(Canvas.TopProperty, (double)y);
        }
    }
    /*public class Boule
    {
        Ellipse boule = new Ellipse()
        {
            Width = 10,
            Height = 10,
            Stroke = Brushes.Brown,
            Fill = Brushes.Brown
        };
        Piste.Children.Add(boule);
    }
    */
}
